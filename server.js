const express = require('express');
const app = express();
const cors = require("cors");
app.use(cors());
app.use(express.urlencoded());
app.use(express.static('front'));

const api = require('./citas.json')

app.get('/', (req, res) => {
    res.send('Api de citas')
})
app.get('/api/citarandom', (req, res) => {
    const randomLimite8 = Math.floor(Math.random() * (1 + 7 - 0)) + 0;
    res.send(api[randomLimite8])
})

const port = process.env.PORT || 3000;
app.listen(port, () => console.log("Escuchando puerto " + port));